﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyCaculator
{
    public partial class Form1 : Form
    {
        InRules calector;
        public Form1()
        {
            InitializeComponent();
            calector = new Calectur();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MessageBox.Show(" کلیه منابع این برنامه متعلق به شرکت پازل میباشد");
        }

        private void btnJam_Click(object sender, EventArgs e)
        {
            if (isTrue())
            {
                int jam=calector.Jame((int)txtAval.Value,(int)txtDovom.Value);
                MessageBox.Show(txtAval.Value+" + "+txtDovom.Value+" = " +jam);
            }
        }

        private void btnTafrigh_Click(object sender, EventArgs e)
        {
            if (isTrue())
            {
                int tafrigh = calector.Tafrigh((int)txtAval.Value, (int)txtDovom.Value);
                MessageBox.Show(txtAval.Value + " - " + txtDovom.Value + " = " + tafrigh);
            }
        }

        private void btnTaghsim_Click(object sender, EventArgs e)
        {
            if (isTrue())
            {
                int taghsim = calector.Taghsim((int)txtAval.Value, (int)txtDovom.Value);
                MessageBox.Show(txtAval.Value + " / " + txtDovom.Value + " = " + taghsim);
            }
        }

        private void btnZarb_Click(object sender, EventArgs e)
        {
            if (isTrue())
            {
                int zarb = calector.Zarb((int)txtAval.Value, (int)txtDovom.Value);
                MessageBox.Show(txtAval.Value + " * " + txtDovom.Value + " = " + zarb);
            }

        }

        private void btnTavan_Click(object sender, EventArgs e)
        {
            if (isTrue())
            {
                int tavan = calector.Tavan((int)txtAval.Value, (int)txtDovom.Value);
                MessageBox.Show(txtAval.Value + " ^ " + txtDovom.Value + " = " + tavan);
            }

        }
        bool  isTrue()
        {
            bool tr = true;
            if (txtAval.Value == 0)
            {
                MessageBox.Show("لطفا عدد اول را به غیر از صفر وارد کنید");
                tr = false;
            }
            else
            {
                if(txtDovom.Value == 0)
                {
                    MessageBox.Show("لطفا عدد دوم را به غیر از صفر وارد کنید");
                    tr = false;
                }
            }
            return tr;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show($" این ماشین حساب فقط از اعداد صحیح استفاده میکند! اتبدا عدد اول و سپس عدد دوم را وارد کنید! و بعد از ان رویه عملیات مورد نظر کلیک کنید");
        }
    }
}
