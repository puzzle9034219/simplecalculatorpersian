﻿namespace MyCaculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnJam = new System.Windows.Forms.Button();
            this.btnTafrigh = new System.Windows.Forms.Button();
            this.btnTaghsim = new System.Windows.Forms.Button();
            this.btnZarb = new System.Windows.Forms.Button();
            this.btnTavan = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtAval = new System.Windows.Forms.NumericUpDown();
            this.txtDovom = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.txtAval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDovom)).BeginInit();
            this.SuspendLayout();
            // 
            // btnJam
            // 
            this.btnJam.Location = new System.Drawing.Point(69, 149);
            this.btnJam.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnJam.Name = "btnJam";
            this.btnJam.Size = new System.Drawing.Size(65, 37);
            this.btnJam.TabIndex = 0;
            this.btnJam.Text = "+";
            this.btnJam.UseVisualStyleBackColor = true;
            this.btnJam.Click += new System.EventHandler(this.btnJam_Click);
            // 
            // btnTafrigh
            // 
            this.btnTafrigh.Location = new System.Drawing.Point(185, 149);
            this.btnTafrigh.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTafrigh.Name = "btnTafrigh";
            this.btnTafrigh.Size = new System.Drawing.Size(65, 37);
            this.btnTafrigh.TabIndex = 1;
            this.btnTafrigh.Text = "-";
            this.btnTafrigh.UseVisualStyleBackColor = true;
            this.btnTafrigh.Click += new System.EventHandler(this.btnTafrigh_Click);
            // 
            // btnTaghsim
            // 
            this.btnTaghsim.Location = new System.Drawing.Point(302, 149);
            this.btnTaghsim.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTaghsim.Name = "btnTaghsim";
            this.btnTaghsim.Size = new System.Drawing.Size(65, 37);
            this.btnTaghsim.TabIndex = 2;
            this.btnTaghsim.Text = "/";
            this.btnTaghsim.UseVisualStyleBackColor = true;
            this.btnTaghsim.Click += new System.EventHandler(this.btnTaghsim_Click);
            // 
            // btnZarb
            // 
            this.btnZarb.Location = new System.Drawing.Point(418, 149);
            this.btnZarb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnZarb.Name = "btnZarb";
            this.btnZarb.Size = new System.Drawing.Size(65, 37);
            this.btnZarb.TabIndex = 3;
            this.btnZarb.Text = "*";
            this.btnZarb.UseVisualStyleBackColor = true;
            this.btnZarb.Click += new System.EventHandler(this.btnZarb_Click);
            // 
            // btnTavan
            // 
            this.btnTavan.Location = new System.Drawing.Point(534, 149);
            this.btnTavan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTavan.Name = "btnTavan";
            this.btnTavan.Size = new System.Drawing.Size(65, 37);
            this.btnTavan.TabIndex = 4;
            this.btnTavan.Text = "^";
            this.btnTavan.UseVisualStyleBackColor = true;
            this.btnTavan.Click += new System.EventHandler(this.btnTavan_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(589, 41);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 29);
            this.label1.TabIndex = 5;
            this.label1.Text = "عدد اول :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(587, 95);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 29);
            this.label2.TabIndex = 6;
            this.label2.Text = "عدد دوم :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(548, 192);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 29);
            this.label3.TabIndex = 9;
            this.label3.Text = "توان";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(82, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 29);
            this.label4.TabIndex = 10;
            this.label4.Text = "جمع";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(193, 192);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 29);
            this.label5.TabIndex = 11;
            this.label5.Text = "تفریق";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(310, 192);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 29);
            this.label6.TabIndex = 12;
            this.label6.Text = "تقسیم";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(426, 192);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 29);
            this.label7.TabIndex = 13;
            this.label7.Text = "ضرب";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(428, 190);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 29);
            this.label8.TabIndex = 13;
            this.label8.Text = "ضرب";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(548, 190);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 29);
            this.label9.TabIndex = 9;
            this.label9.Text = "توان";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(550, 190);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 29);
            this.label10.TabIndex = 9;
            this.label10.Text = "توان";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(310, 192);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 29);
            this.label11.TabIndex = 12;
            this.label11.Text = "تقسیم";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(311, 190);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 29);
            this.label12.TabIndex = 12;
            this.label12.Text = "تقسیم";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(196, 190);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 29);
            this.label13.TabIndex = 11;
            this.label13.Text = "تفریق";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(85, 190);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 29);
            this.label14.TabIndex = 10;
            this.label14.Text = "جمع";
            // 
            // txtAval
            // 
            this.txtAval.Location = new System.Drawing.Point(26, 39);
            this.txtAval.Name = "txtAval";
            this.txtAval.Size = new System.Drawing.Size(556, 37);
            this.txtAval.TabIndex = 14;
            // 
            // txtDovom
            // 
            this.txtDovom.Location = new System.Drawing.Point(26, 93);
            this.txtDovom.Name = "txtDovom";
            this.txtDovom.Size = new System.Drawing.Size(556, 37);
            this.txtDovom.TabIndex = 15;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Nazanin", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button1.Location = new System.Drawing.Point(622, 196);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(43, 25);
            this.button1.TabIndex = 16;
            this.button1.Text = "راهنما";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 230);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtDovom);
            this.Controls.Add(this.txtAval);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnTavan);
            this.Controls.Add(this.btnZarb);
            this.Controls.Add(this.btnTaghsim);
            this.Controls.Add(this.btnTafrigh);
            this.Controls.Add(this.btnJam);
            this.Font = new System.Drawing.Font("Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ماشین حساب";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtAval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDovom)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnJam;
        private System.Windows.Forms.Button btnTafrigh;
        private System.Windows.Forms.Button btnTaghsim;
        private System.Windows.Forms.Button btnZarb;
        private System.Windows.Forms.Button btnTavan;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown txtAval;
        private System.Windows.Forms.NumericUpDown txtDovom;
        private System.Windows.Forms.Button button1;
    }
}

